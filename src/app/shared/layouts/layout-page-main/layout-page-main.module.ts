import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterModule } from '../../components/footer/footer.module';
import { NavbarModule } from '../../components/navbar/navbar.module';
import { SocialShareModule } from '../../components/social-share/social-share.module';
import { LayoutPageMainComponent } from './layout-page-main.component';

@NgModule({
  declarations: [
    LayoutPageMainComponent,
  ],
  exports: [
    LayoutPageMainComponent,
  ],
  imports: [
    CommonModule,
    NavbarModule,
    FooterModule,
    SocialShareModule,
  ],
})
export class LayoutPageMainModule { }
