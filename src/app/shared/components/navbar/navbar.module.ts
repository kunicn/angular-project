import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCollapseModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from '../../../app-routing.module';
import { DarkModeToggleModule } from '../dark-mode-toggle/dark-mode-toggle.module';
import { NavbarComponent } from './navbar.component';

@NgModule({
  declarations: [
    NavbarComponent,
  ],
  exports: [
    NavbarComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
    NgbCollapseModule,
    DarkModeToggleModule,
  ]
})
export class NavbarModule { }
