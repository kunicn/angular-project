import { Component, OnInit } from '@angular/core';
import { SocShare } from './soc-share';

@Component({
  selector: 'app-social-share',
  templateUrl: './social-share.component.html',
  styleUrls: ['./social-share.component.scss']
})
export class SocialShareComponent implements OnInit {

  socShares = [
    new SocShare (
      'assets/images/social-share/facebook.png',
      'https://www.facebook.com/',
      'Facebook'
    ),
    new SocShare (
      'assets/images/social-share/instagram.png',
      'https://www.instagram.com/',
      'Instagram'
    ),
    new SocShare (
      'assets/images/social-share/twitter.png',
      'https://twitter.com/',
      'Twitter'
    ),
    new SocShare (
      'assets/images/social-share/pinterest.png',
      'https://www.pinterest.com/',
      'Pinterest'
    )
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
