import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocialShareComponent } from './social-share.component';

@NgModule({
  declarations: [
    SocialShareComponent
  ],
  exports: [
    SocialShareComponent
  ],
  imports: [
    CommonModule
  ]
})
export class SocialShareModule { }
