import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from '../../../app-routing.module';
import { HeroComponent } from './hero.component';

@NgModule({
  declarations: [
    HeroComponent,
  ],
  exports: [
    HeroComponent,
  ],
  imports: [
    CommonModule,
    AppRoutingModule,
  ]
})
export class HeroModule { }
