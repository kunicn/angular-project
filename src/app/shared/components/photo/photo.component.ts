import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.scss']
})
export class PhotoComponent implements OnInit {

  @Input() photoUrl: string | undefined;
  @Input() altText: string | undefined;

  constructor() { }

  ngOnInit(): void {
  }

}
