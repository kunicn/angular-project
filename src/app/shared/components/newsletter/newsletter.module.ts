import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { NewsletterComponent } from './newsletter.component';

@NgModule({
  declarations: [
    NewsletterComponent,
  ],
  exports: [
    NewsletterComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
  ]
})
export class NewsletterModule { }
