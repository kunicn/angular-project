import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  title = 'Frontend Interview Test';
  subtitle = 'Angular project with SCSS & Bootstrap';

  constructor() { }

  ngOnInit(): void {
  }

}
