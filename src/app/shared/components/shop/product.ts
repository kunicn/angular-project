export class Product {
  constructor(
    public id: number,
    public name: string,
    public imageUrl: string,
    public category: string,
    public imageByCategoryUrl: string,
    public price: number,
    public size: any,
    public tagNew: boolean,
    public tagBestSeller: boolean,
    public tagSale: boolean) { }
}
