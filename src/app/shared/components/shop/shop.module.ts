import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbNavModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from '../../../app-routing.module';
import { ShopComponent } from './shop.component';

@NgModule({
  declarations: [
    ShopComponent,
  ],
  exports: [
    ShopComponent,
  ],
  imports: [
    CommonModule,
    NgbNavModule,
    AppRoutingModule,
  ],
})
export class ShopModule { }
