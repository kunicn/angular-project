import { Component, Input } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, style, animate, transition } from '@angular/animations';
import { Product } from './product';

@Component({
  selector: 'app-shop',
  templateUrl: './shop.component.html',
  styleUrls: ['./shop.component.scss'],

  animations: [

    /* FADE IN ANIMATION */
    trigger('fadeInAnimation', [
      state('void', style({
        opacity: 0
      })),
      state('*', style({
        opacity: 1
      })),
      transition('void <=> *', animate(1000))
    ]),

    /*HOVER BORDER ANIMATION */
    trigger('hoverBorder', [
      state('normal', style({
        'border-color': 'transparent'
      })),
      state('marked', style({
        'border-color': '#6c854b'
      })),
      transition('normal <=> marked', animate(300))
    ])

  ]

})
export class ShopComponent {

  @Input() isHomePage: boolean | undefined;
  @Input() isShopPage: boolean | undefined;
  @Input() hasTag: boolean | undefined;
  @Input() tagNewLabel: string | undefined;


  /* MODAL */
  closeResult: string | undefined;
  constructor(private modalService: NgbModal) {}

  /* DATA */
  public products = [
    new Product(
      1,
      'Night table',
      'assets/images/shop/featured/night-table.jpg',
      'bedroom',
      'assets/images/shop/categories/bedroom-night-table.jpg',
      9500,
      ['small', 'medium', 'large'],
      true,
      false,
      false
    ),
    new Product(
      2,
      'Table',
      'assets/images/shop/featured/table.jpg',
      'living-room',
      'assets/images/shop/categories/living-room-table.jpg',
      40000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      3,
      'Sofa',
      'assets/images/shop/featured/sofa.jpg',
      'living-room',
      'assets/images/shop/categories/living-room-sofa.jpg',
      40000,
      ['small', 'medium', 'large'],
      false,
      false,
      true
    ),
    new Product(
      4,
      'Chair',
      'assets/images/shop/featured/chair.jpg',
      'living-room',
      'assets/images/shop/categories/living-room-chair.jpg',
      5000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      5,
      'Kitchen',
      'assets/images/shop/featured/kitchen.jpg',
      'kitchen',
      'assets/images/shop/categories/kitchen-kitchen.jpg',
      89000,
      ['small', 'medium', 'large'],
      false,
      true,
      false
    ),
    new Product(
      6,
      'Bedroom',
      'assets/images/shop/featured/bedroom.jpg',
      'bedroom',
      'assets/images/shop/categories/bedroom-bedroom.jpg',
      350000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      7,
      'Armchair',
      'assets/images/shop/featured/armchair.jpg',
      'living-room',
      'assets/images/shop/categories/living-room-armchair.jpg',
      9000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      8,
      'Bar stool',
      'assets/images/shop/featured/bar-stool.jpg',
      'kitchen',
      'assets/images/shop/categories/kitchen-bar-stool.jpg',
      3000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      9,
      'Cosy couch',
      'assets/images/shop/featured/cosy-couch.jpg',
      'living-room',
      'assets/images/shop/categories/living-room-cosy-couch.jpg',
      350000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      10,
      'Dining table',
      'assets/images/shop/featured/dining-table.jpg',
      'kitchen',
      'assets/images/shop/categories/kitchen-dining-table.jpg',
      50000,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
    new Product(
      11,
      'Pillow',
      'assets/images/shop/featured/pillow.jpg',
      'living-room',
      'assets/images/shop/categories/living-room-pillow.jpg',
      1125,
      ['small', 'medium', 'large'],
      false,
      false,
      false
    ),
  ];


  /* FILTERED CATEGORIES OF PRODUCTS */
  livingRoomProducts = this.products.filter((product) => {
    return product.category === 'living-room';
  });

  bedRoomProducts = this.products.filter((product) => {
    return product.category === 'bedroom';
  });

  kitchenProducts = this.products.filter((product) => {
    return product.category === 'kitchen';
  });

  bathRoomProducts = this.products.filter((product) => {
    return product.category === 'bathroom';
  });

  /* ANIMATION */
  currentState: string | undefined;


  /* TABS*/
  active = 1;


  /* ANIMATION */
  animate() {
    this.currentState === 'normal' ? this.currentState = 'marked' : this.currentState = 'normal';
  }

  /* MODAL */
  open(content: any) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

}
