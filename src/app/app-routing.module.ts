import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutPageComponent } from './views/about-page/about-page.component';
import { ContactPageComponent } from './views/contact-page/contact-page.component';
import { HomePageComponent } from './views/home-page/home-page.component';
import { LoginPageComponent } from './views/login-page/login-page.component';
import { ShopPageComponent } from './views/shop-page/shop-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  {
    path: 'home',
    component: HomePageComponent,
    data: {animation: 'Home'}
  },
  {
    path: 'about',
    component: AboutPageComponent,
    data: {animation: 'About'}
  },
  {
    path: 'shop',
    component: ShopPageComponent,
    data: {animation: 'Shop'}
  },
  {
    path: 'contact',
    component: ContactPageComponent,
    data: {animation: 'Contact'}
  },
  {
    path: 'login',
    component: LoginPageComponent,
    data: {animation: 'Login'}
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
