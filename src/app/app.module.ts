import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactFormModule } from './shared/components/contact-form/contact-form.module';
import { DescriptionModule } from './shared/components/description/description.module';
import { FooterModule } from './shared/components/footer/footer.module';
import { HeroModule } from './shared/components/hero/hero.module';
import { LoginFormModule } from './shared/components/login-form/login-form.module';
import { MapModule } from './shared/components/map/map.module';
import { NavbarModule } from './shared/components/navbar/navbar.module';
import { NewsletterModule } from './shared/components/newsletter/newsletter.module';
import { PhotoModule } from './shared/components/photo/photo.module';
import { ShopModule } from './shared/components/shop/shop.module';
import { SocialShareModule } from './shared/components/social-share/social-share.module';
import { LayoutPageMainModule } from './shared/layouts/layout-page-main/layout-page-main.module';
import { HomePageComponent } from './views/home-page/home-page.component';
import { AboutPageComponent } from './views/about-page/about-page.component';
import { ContactPageComponent } from './views/contact-page/contact-page.component';
import { ShopPageComponent } from './views/shop-page/shop-page.component';
import { LoginPageComponent } from './views/login-page/login-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    AboutPageComponent,
    ContactPageComponent,
    ShopPageComponent,
    LoginPageComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    NavbarModule,
    HeroModule,
    MapModule,
    DescriptionModule,
    SocialShareModule,
    ContactFormModule,
    NewsletterModule,
    FooterModule,
    PhotoModule,
    ShopModule,
    BrowserAnimationsModule,
    LoginFormModule,
    LayoutPageMainModule,
  ],
  providers: [],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
